package com.somospnt.mockito;

public class StaticUtils {

    public static String saludar() {
        return "Hola, mundo!";
    }

    public static String saludar(String nombre) {
        return "Hola, " + nombre + "!";
    }
}
