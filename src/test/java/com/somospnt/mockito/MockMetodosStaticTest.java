package com.somospnt.mockito;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.MockedStatic;
import static org.mockito.Mockito.*;

/**
 * También se puede realizar mocks sobre métodos estáticos. El proceso es un
 * poco más complicado, ya que involucra no solo la creación del mock, sino la
 * liberación del mismo (para que se "deshaga" el cambio del método estático).
 */
public class MockMetodosStaticTest {

    /**
     * El caso más básico es mockear un método estático sin parámetros. Es muy
     * importante liberar el mock una vez creado, por eso el try-with-resources.
     */
    @Test
    public void mockMetodoStaticSinParametros() {
        // El metodo estatico funciona normalmente
        assertEquals("Hola, mundo!", StaticUtils.saludar());

        // Es muy importante "liberar" el mock al terminar de usarlo, por eso el try-with-resources
        try (MockedStatic<StaticUtils> mocked = mockStatic(StaticUtils.class)) {
            mocked.when(StaticUtils::saludar).thenReturn("Foo");
            assertEquals("Foo", StaticUtils.saludar());
        }

        // Nuevamente vuelve a funcionar normal
        assertEquals("Hola, mundo!", StaticUtils.saludar());
    }

    /**
     * Dependiendo la versión de Java, si el try-with-resources no está
     * soportado debemos cerrarlo manualmente con un clasico try-finally.
     */
    @Test
    public void mockMetodosStaticConCerradoManual() {
        // El metodo estatico funciona normalmente
        assertEquals("Hola, mundo!", StaticUtils.saludar());

        // Es muy importante "liberar" el mock al terminar de usarlo, por eso el finally
        MockedStatic<StaticUtils> mocked = null;
        try {
            mocked = mockStatic(StaticUtils.class);
            mocked.when(StaticUtils::saludar).thenReturn("Foo");
            assertEquals("Foo", StaticUtils.saludar());
        } finally {
            mocked.close();
        }

        // Nuevamente vuelve a funcionar normal
        assertEquals("Hola, mundo!", StaticUtils.saludar());
    }

    /**
     * Cuando hay parámetros, usamos un lambda para realizar la invocacion.
     */
    @Test
    public void mockMetodoStaticConParametros() {
        // El metodo estatico funciona normalmente
        assertEquals("Hola, Bar!", StaticUtils.saludar("Bar"));

        // Es muy importante "liberar" el mock al terminar de usarlo, por eso el try-with-resources
        try (MockedStatic<StaticUtils> mocked = mockStatic(StaticUtils.class)) {
            mocked.when(() -> StaticUtils.saludar("Bar")).thenReturn("Foo");
            assertEquals("Foo", StaticUtils.saludar("Bar"));
        }

        // Nuevamente vuelve a funcionar normal
        assertEquals("Hola, Bar!", StaticUtils.saludar("Bar"));
    }

    /**
     * Si usamos una versión de Java sin soporte para Lambdas, recurrimos a la
     * implementación inline de la interfaz.
     */
    @Test
    public void mockMetodoStaticConParametrosSinLambda() {
        // El metodo estatico funciona normalmente
        assertEquals("Hola, Bar!", StaticUtils.saludar("Bar"));

        // Es muy importante "liberar" el mock al terminar de usarlo
        MockedStatic<StaticUtils> mocked = null;
        try {
            mocked = mockStatic(StaticUtils.class);
            mocked.when(new MockedStatic.Verification() {
                @Override
                public void apply() throws Throwable {
                    StaticUtils.saludar("Bar");
                }
            }).thenReturn("Foo");

            assertEquals("Foo", StaticUtils.saludar("Bar"));
        } finally {
            mocked.close();
        }

        // Nuevamente vuelve a funcionar normal
        assertEquals("Hola, Bar!", StaticUtils.saludar("Bar"));
    }

}
