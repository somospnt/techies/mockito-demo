package com.somospnt.mockito;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Creación de un spy.
 * Los spy se usan para "espiar" objetos reales. A difererncia de los mock, los
 * spy invocan a los métodos reales (a menos que se les haga un stub).
 * En general se prefiere el uso de mocks.
 *
 */
public class SpySimpleTest {

    @Test
    public void spySimple() {
        ArrayList mockedList = spy();

        // se invocan los metodos reales
        mockedList.add("uno");
        mockedList.add("dos");

        // tambien se puede crear un stub
        when(mockedList.size()).thenReturn(1234);

        // verificamos
        assertEquals("uno", mockedList.get(0));
        assertEquals("dos", mockedList.get(1));
        assertEquals(1234, mockedList.size());

        // tambien se puede verificar que se hayan invocado los metodos
        verify(mockedList).add("uno");
        verify(mockedList).add("dos");
    }


}
