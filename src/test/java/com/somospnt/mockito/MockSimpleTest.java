package com.somospnt.mockito;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

public class MockSimpleTest {

    /**
     * Creación de un mock.
     * Los mock son objetos "falsos", que no tienen ningún comportamiento.
     * Los objetos mock reaccionan en silencio, no realizan ninguna operación.
     * Si el método retorna un valor, al ser un mock retornará null, 0 o lista vacia.
     * Se le puede asociar retornos especificos a ciertos metodos (stubbing).
     */
    @Test
    public void mockSimple() {
        List mockedList = mock();

        // Cuando usamos el mock, va reaccionando silenciosamente.
        mockedList.add("uno");
        mockedList.add("dos");
        mockedList.clear();

        // verificamos que se hayan invocado los métodos
        verify(mockedList).add("uno");
        verify(mockedList).add("dos");
        verify(mockedList).clear();
    }

    /**
     * El stubbing es la acción de agregarle comportamiento al mock.
     * De esta forma, indicamos qué debe devolver un mock cuando se le invoque algún método.
     */
    @Test
    public void stubbing() {
        List mockedList = mock();

        // Le damos comportamiento especifico a los metodos del mock
        when(mockedList.get(0)).thenReturn("primero");
        when(mockedList.get(1)).thenReturn("segundo");
        when(mockedList.get(2)).thenThrow(new RuntimeException("Excepcion forzada"));

        // Verificamos el comportamiento
        assertEquals("primero", mockedList.get(0));
        assertEquals("segundo", mockedList.get(1));
        assertNull(mockedList.get(333)); //Este parametro 333 no tiene comportamiento asignado, asi que retorna null
        try {
            mockedList.get(2);
            fail("La linea anterior debería lanzar excepcion");
        } catch (RuntimeException ex) {
            assertEquals("Excepcion forzada", ex.getMessage());
        }
    }

    /**
     * Podemos también realizar el stubbing para cualquier valor de parámetros
     * que se envie al método (estilo "comodín).
     */
    @Test
    public void stubbingConArgumentMatching() {
        List<String> mockedList = mock();

        // Para cualquier int (anyInt) que se le pase al método get siempre retorna el mismo String.
        // Revisar tambien anyString(), anyLong(), anyList(), etc.
        when(mockedList.get(anyInt())).thenReturn("un elemento de la lista");

        // Verificamos el comportamiento
        assertEquals("un elemento de la lista", mockedList.get(231));
        assertEquals("un elemento de la lista", mockedList.get(0));
        assertEquals("un elemento de la lista", mockedList.get(987541));
    }

    /**
     * Cuando el metodo no tiene retorno y lanza una excepcion, su stubbing
     * es diferente;
     */
    @Test
    public void stubbingDeMetodosVoidConExcepciones() {
        List mockedList = mock();

        doThrow(new RuntimeException("Se invoco al clear")).when(mockedList).clear();

        try {
            mockedList.clear();
            fail("La linea anterior debería lanzar excepcion");
        } catch (RuntimeException ex) {
            assertEquals("Se invoco al clear", ex.getMessage());
        }
    }

    /**
     * A veces resulta útil derivar la invocación de un método particular a su
     * implementación real.
     */
    @Test
    public void stubbingInvocandoMetodoReal() {
        List mockedList = mock();

        when(mockedList.toString()).thenCallRealMethod();

        System.out.println(mockedList.toString());
    }

}
