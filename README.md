# mockito-demo

Proyecto con ejemplos simples de uso de Mockito.

# Clases de ejemplo

Los ejemplos se encuentran ordenados en clases de test. Se recomienda revisar
estas clases en el siguiente orden:

1. MockSimpleTest
2. MockMetodosStaticTest
3. SpySimpleTest

# Información útil

Web oficial:
https://site.mockito.org/

Documentación y ejemplos:
https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html

Artículos:
https://www.adictosaltrabajo.com/2009/01/29/mockito/

